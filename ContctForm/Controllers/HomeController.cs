﻿using System;
using ContctForm.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using RestSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Caching;

namespace ContctForm.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {

            ViewBag.AgeList = GetAgeList();
            return View();
        }

        [HttpGet]
        public ActionResult ThankYou()
        {
            ContactInfo _contactInfo = (ContactInfo)TempData["contactInfo"];
            ViewBag.AgeValue =(_contactInfo == null? string.Empty: GetAgeList().Find(m => m.Value == _contactInfo.Age).Text);
            return View(_contactInfo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(ContactInfo _contactInfo)
        {
            ViewBag.ErrorMessage = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(_contactInfo.Email))
                {
                    string emailRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                                             @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                                                @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
                    Regex re = new Regex(emailRegex);
                    if (!re.IsMatch(_contactInfo.Email))
                    {
                        ModelState.AddModelError("Email", "Email is not valid");
                    }
                }
                else
                {
                    ModelState.AddModelError("Email", "Email is required");
                }

                if (ModelState.IsValid)
                {
                    TempData["contactInfo"] = _contactInfo;
                    return RedirectToAction("ThankYou", "Home");
                }
            }
            catch(Exception ex) {
                ViewBag.ErrorMessage = "Contact form submission Failed";
            }

            ViewBag.AgeList = GetAgeList();
            return View(_contactInfo);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        private List<SelectListItem> GetAgeList()
        {
            if (HttpRuntime.Cache != null && (List<SelectListItem>) HttpRuntime.Cache["AgeList"] != null)
                return (List<SelectListItem>) HttpRuntime.Cache["AgeList"];

            List<SelectListItem> ageList = new List<SelectListItem>();
            var client = new RestClient("https://coderbyte.com/");
            var request = new RestRequest("api/challenges/json/age-counting", DataFormat.Json);
            var response = client.Get(request);
            dynamic res = JsonConvert.DeserializeObject(response.Content);

            string jsonResponse = res.data.Value.ToString();
            string pattern = @"key=([^,]+),\s+age=([\d]+)";

            foreach (Match match in Regex.Matches(jsonResponse, @"key=([^,]+),\s+age=([\d]+)"))
            {
                if (ageList.Any(m => m.Text == match.Groups[2].Value))
                    ageList.Remove(ageList.Find(m => m.Text == match.Groups[2].Value));

                ageList.Add(new SelectListItem { Value = match.Groups[1].Value, Text = match.Groups[2].Value });
            }

            return ageList.OrderBy(m => Convert.ToInt16(m.Text)).ToList();
        }
    }
}