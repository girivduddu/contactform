﻿Project notes
=============
This application is using .Net framework 4.7.2. Please make sure the app pool has .Net framework 4

Requirements
============
Article type: Project
Instructions
Overview
The purpose of this project is to test your skills with C# and ASP.NET MVC. By no means does visual rendering or front end performance impact your final score. Instead, the focus should be on clean, secure code that is well-documented and easy to set up.

How to submit
Upload your completed project to your Bitbucket, and then paste a link to the repository below in the form along with any comments you have about your solution.

Assessment Instructions
Create a basic MVC web application project in Visual Studio. This project can follow any solution structure you wish, as long as it is easy to navigate to any user.
Include a README of necessary steps to set up and install your application.
The application must include a publish profile using Web Deploy to a local IIS website.
The home page should include a simple contact form. The form must include the following fields:
First name
Last name
Email address
Phone number
Age
Make this a drop down field.
The drop down options are based on the data found at https://coderbyte.com/api/challenges/json/age-counting
The API data will need to be parsed as key/age pairs.
For duplicate ages, use the option with the highest key/age index in the entire data set from the API.
The text of the drop down option is the "age" parameter.
The value of the drop down option is the "key" parameter.
Contact message
Upon submission of the contact form, you must perform backend validation on the email format.
If the form is valid, you must redirect the user to a dedicated thank you page.
The thank you page should include a re-gurgitation of the form submission details from the user.
Using the value of the Age field, include the index of the key found in the API.
Ensure the solution is secure from cross-site scripting
Submissions (0)
Submit your answer


